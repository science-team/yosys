#!/bin/bash

DATE=$(LC_ALL=C date -u -d @$SOURCE_DATE_EPOCH +"%d %B %Y")

txt2man -d "$DATE" -t YOSYS-SMTBMC -s 1 yosys-smtbmc.txt > yosys-smtbmc.1
txt2man -d "$DATE" -t YOSYS-SMTBMC -s 1 yosys-witness.txt > yosys-witness.1
txt2man -d "$DATE" -t YOSYS-ABC -s 1 yosys-abc.txt > yosys-abc.1
