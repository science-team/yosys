Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: yosys
Source: http://www.clifford.at/yosys/
Files-Excluded-abc:
 lib/x64/*
 lib/x86/*
 .gitattributes

Files: *
Copyright:
 2012-2019 Clifford Wolf <clifford@clifford.at>
 2018-2022 Miodrag Milanovic <micko@yosyshq.com>
 2021-2022 Marcelina Kościelnicka <mwk@0x04.net>
 2020 Alberto Gonzalez <boqwxp@airmail.cc>
 whitequark <whitequark@whitequark.org>
License: ISC

Files: backends/btor/btor.cc
Copyright: 2012 Clifford Wolf <clifford@clifford.at>
           2014 Ahmed Irfan <irfan@fbk.eu>
License: ISC

Files: passes/cmds/tee.cc passes/tests/test_cell.cc passes/cmds/logcmd.cc
       passes/cmds/write_file.cc passes/cmds/trace.cc
       passes/tests/test_abcloop.cc
Copyright: 2012-2014 Clifford Wolf <clifford@clifford.at>
           2014 Johann Glaser <Johann.Glaser@gmx.at>
License: ISC

Files: techlibs/ecp5/arith_map.v
       techlibs/ecp5/synth_ecp5.cc
       techlibs/gowin/arith_map.v
Copyright: 2012 Clifford Wolf <clifford@clifford.at>
           2018-2019 David Shah <dave@ds0.me>
License: ISC

Files: frontends/aiger/aigerparse.*
Copyright: 2012 Clifford Wolf <clifford@clifford.at>
           2012 Eddie Hung <eddie@fpgeh.com>
License: ISC

Files: frontends/ast/ast.cc
       passes/hierarchy/hierarchy.cc
Copyright: 2012 Clifford Wolf <clifford@clifford.at>
           2018 Ruben Undheim <ruben.undheim@gmail.com>
License: ISC

Files: passes/opt/muxpack.cc
Copyright: 2012 Clifford Wolf <clifford@clifford.at>
           2019 Eddie Hung <eddie@fpgeh.com>
License: ISC

Files: passes/cmds/bugpoint.cc
       passes/equiv/equiv_opt.cc
       passes/opt/opt_lut.cc
       passes/techmap/flowmap.cc
Copyright: 2018 whitequark <whitequark@whitequark.org>
License: ISC

Files: passes/techmap/extract_reduce.cc
       techlibs/coolrunner2/*
Copyright: 2017 Robert Ou <rqou@robertou.com>
License: ISC

Files: techlibs/anlogic/anlogic_eqn.cc
       techlibs/anlogic/arith_map.v
       techlibs/anlogic/synth_anlogic.cc
Copyright: 2012,2018 Clifford Wolf <clifford@clifford.at
           2018 Icenowy Zheng <icenowy@aosc.io>
           2018 Miodrag Milanovic <miodrag@symbioticeda.com>
License: ISC

Files:
 techlibs/lattice/arith_map_ccu2c.v
 techlibs/lattice/synth_lattice.cc
Copyright:
 2012  Claire Xenia Wolf <claire@yosyshq.com>
 2018  gatecat <gatecat@ds0.me>
License: ISC

Files: libs/minisat/*
Copyright: 2003-2006, Niklas Een, Niklas Sorensson
           2006-2012  Niklas Sorensson
License: MIT

Files: libs/sha1/*
Copyright: Steve Reid <steve@edmweb.com>
           Bruce Guenter <bruce@untroubled.org>
           Volker Grabsch <vog@notjusthosting.com>
License: Public-Domain
 These files are in the public domain.

Files: libs/fst/*
Copyright:
 2005-2007 Ariya Hidayat (ariya@kde.org)
 2009-2018 Tony Bybell
License: MIT

Files: libs/fst/lz4.*
Copyright: 2011-2015, Yann Collet
License: BSD-2-Clause
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
   .
       * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
       * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following disclaimer
   in the documentation and/or other materials provided with the
   distribution.
   .
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: misc/launcher.c
Copyright: 2016 Jason R Coombs <jaraco@jaraco.com>
License: MIT

Files: tests/hana/hana_vlib.v
Copyright: 2009-2010 Parvez Ahmad <parvez_ahmad@yahoo.co.uk>
License: GPL-3+

Files: passes/fsm/fsm_export.cc
Copyright: 2012 Clifford Wolf <clifford@clifford.at>
           2012 Martin Schmölzer <martin@schmoelzer.at>
License: ISC

Files: tests/memories/amber23_sram_byte_en.v
Copyright: 2010 Conor Santifort <csantifort.amber@gmail.com>
           2010 OPENCORES.ORG
License: LGPL-2.1+



Files: abc/*
Copyright: The Regents of the University of California
License: University-of-California-Berkeley

Files: abc/src/bdd/cudd/* abc/src/bdd/mtr/* abc/src/bdd/epd/*
Copyright: 1995-2004 Regents of the University of Colorado
License: University-of-Colorado

Files: abc/lib/pthread.h abc/lib/sched.h abc/lib/semaphore.h
Copyright: 1998 John E. Bossom
           1999,2005 Pthreads-win32 contributors
License: GPL-2+

Files: abc/src/sat/lsat/solver.h
Copyright: 2008 Niklas Sorensson
           2008 Koen Claessen
License: MIT

Files: abc/src/sat/bsat/*
Copyright: 2005 Niklas Sorensson
License: MIT

Files: abc/src/sat/bsat2/*
       abc/src/sat/glucose/*
Copyright: 2003-2006 Niklas Een, Niklas Sorensson
           2006-2010 Niklas Sorensson
           2009 Gilles Audemard
           2009,2013 Laurent Simon
License: MIT

Files: abc/src/sat/satoko/*
Copyright: 2017 Bruno Schmitt - UC Berkeley / UFRGS <bruno@oschmitt.com>
License: BSD-2

Files: abc/src/sat/xsat/*
Copyright: 2003-2006 Niklas Een, Niklas Sorensson
           2006-2010 Niklas Sorensson
           2009 Gilles Audemard
           2009,2013 Laurent Simon
           2016 Bruno Schmitt - UC Berkeley / UFRGS <bruno@oschmitt.com>
License: MIT

Files: abc/src/sat/bsat2/pstdint.h
       abc/src/sat/glucose/pstdint.h
Copyright: 2005-2014 Paul Hsieh
License: BSD-3

Files: abc/src/misc/bzlib/*
Copyright: 1996-2007 Julian Seward <jseward@bzip.org>
License: BSD-4

Files: abc/src/misc/zlib/*
Copyright: 1995-2010 Mark Adler
           1995-2010 Jean-loup Gailly
License: zlib-license

Files: abc/src/base/abci/abcSaucy.c
Copyright: 2004 The Regents of the University of Michigan
License: University-of-Michigan



Files: debian/*
Copyright: 2014-2016,2018-2019 Ruben Undheim <ruben.undheim@gmail.com>
           2016 Sebastian Kuzminsky <seb@highlab.com>
	   2023 Daniel Gröber <dxld@darkboxed.org>
License: GPL-2+

Files: debian/patches/*
Copyright: 2023 Daniel Gröber <dxld@darkboxed.org>
License: ISC

Files: debian/patches/abc/*
Copyright: 2014-2016,2018-2019 Ruben Undheim <ruben.undheim@gmail.com>
License: GPL-2+

Files:
 debian/patches/kfreebsd-support.patch
 debian/patches/0007-Disable-pretty-build.patch
 debian/patches/0009-Some-spelling-errors-fixed.patch
 debian/patches/0010-Fix-adding-of-sys.path-in-yosys-smtbmc.patch
 debian/patches/0011-Do-not-show-g-build-flags-in-Version-string.patch
 debian/patches/0018-Fix-autotest-compliation.patch
 debian/patches/0020-autotest-Print-log-on-error.patch
Copyright: 2014-2016,2018-2019 Ruben Undheim <ruben.undheim@gmail.com>
           2016 Sebastian Kuzminsky <seb@highlab.com>
License: GPL-2+

Files:
 debian/patches/0017-Support-plugin-loading-from-libdir.patch
Copyright: 2023 Daniel Gröber <dxld@darkboxed.org>
License: ISC

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any   
 purpose with or without fee is hereby granted, provided that the above     
 copyright notice and this permission notice appear in all copies.          
 .                                                                          
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES   
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF           
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR    
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES     
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN      
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF    
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.             

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either version 2.1
 og the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the 
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian Systems, the full text of the license can be found on
 /usr/share/common-licenses/LGPL-2.1

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: University-of-California-Berkeley
 Permission is hereby granted, without written agreement and without license or
 royalty fees, to use, copy, modify, and distribute this software and its
 documentation for any purpose, provided that the above copyright notice and
 the following two paragraphs appear in all copies of this software.
 .
 IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
 THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
 CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS,
 AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO PROVIDE MAINTENANCE,
 SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

License: University-of-Colorado 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 Neither the name of the University of Colorado nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.]


License: zlib-license
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: University-of-Michigan
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 Neither the name of the University of Michigan nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.]

License: BSD-2
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF

License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF


License: BSD-4
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. The origin of this software must not be misrepresented; you must 
    not claim that you wrote the original software.  If you use this 
    software in a product, an acknowledgment in the product 
    documentation would be appreciated but is not required.
 .
 3. Altered source versions must be plainly marked as such, and must
    not be misrepresented as being the original software.
 .
 4. The name of the author may not be used to endorse or promote 
    products derived from this software without specific prior written 
    permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
